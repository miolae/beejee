<?php

namespace controllers;

use core\Application;
use core\components\Controller;
use models\Task as TaskModel;

class Task extends Controller
{
    public function actionIndex()
    {
        if (empty($_GET['orderby']['field'])) {
            $field = 'author';
        } else {
            if (in_array($_GET['orderby']['field'], ['author', 'email', 'ready'], true)) {
                $field = $_GET['orderby']['field'];
            } else {
                $field = 'author';
            }
        }

        if (empty($_GET['orderby']['order'])) {
            $order = 'asc';
        } else {
            if (in_array($_GET['orderby']['order'], ['author', 'email', 'ready'], true)) {
                $order = $_GET['orderby']['order'];
            } else {
                $order = 'asc';
            }
        }

        $tasks = TaskModel::find()->order($field, $order)->all();

        return [
            'tasks'   => $tasks,
            'isGuest' => Application::$instance->user->isGuest(),
        ];
    }

    public function actionShow()
    {
        $id = (int)$_GET['id'];

        if ($id <= 0 || !$task = TaskModel::find()->where("id = $id")->one()) {
            Application::$instance->view->render('', 404);

            return false;
        }

        return [
            'task'   => $task,
            'isGuest' => Application::$instance->user->isGuest(),
        ];
    }

    public function actionEdit()
    {
        $id = (int)$_GET['id'];
        $result = [];

        if ($id > 0) {
            if (Application::$instance->user->isGuest()) {
                Application::$instance->view->render('', 404);

                return false;
            }

            $task = TaskModel::find()->where("id = $id")->one();
        } else {
            $task = new TaskModel();
        }

        /** @var TaskModel $task */

        if (!empty($_POST)) {
            if (!empty($_POST['author'])) {
                $task->author = $_POST['author'];
            }
            if (!empty($_POST['email'])) {
                $task->email = $_POST['email'];
            }
            if (!empty($_POST['message'])) {
                $task->message = $_POST['message'];
            }
            if (isset($_POST['ready'])) {
                $task->ready = ($_POST['ready'] ? 1 : 0);
            }

            if ($task->save()) {
                Application::$instance->redirect('/');
            } else {
                $result['error'] = 'Проверьте правильность заполнения полей';
            }
        }

        $result['task'] = $task;

        return $result;
    }
}
