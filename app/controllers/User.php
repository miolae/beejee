<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 18.07.2017
 * Time: 20:28
 */

namespace controllers;

use core\Application;
use core\components\Controller;

class User extends Controller
{
    public function actionLogin()
    {
        $user = Application::$instance->user;
        $result = [];

        if (!empty($_POST['login'])) {
            if ($user->authorize($_POST['login'], $_POST['password'])) {
                Application::redirect('/');
            } else {
                $result['error'] = 'Неверный логин или пароль';
            }
        }

        $result['isGuest'] = $user->isGuest();

        return $result;
    }

    public function actionLogout()
    {
        Application::$instance->user->logout();
        Application::redirect('/');
    }
}
