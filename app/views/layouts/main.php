<?php
/** @var \core\components\View $this */
$this->addCss('bootstrap.min.css');
$this->addCss('style.css');

$user = \core\Application::$instance->user;
?>

<html>
<head>
    <title>BeeJee test case</title>
    <? $this->showAssets(); ?>
</head>
<body>
<header>
    <div class="row">
        <div class="col-md-12">
            <a href="/"><h4 class="pull-left">BeeJee test case</h4></a>
            <? if ($user->isGuest()): ?>
                <a href="/?c=user&a=login" class="pull-right">Войти</a>
            <? else: ?>
                <div class="pull-right">
                    <?= htmlspecialchars($user->model->login) ?>,
                    <a href="/?c=user&a=logout">Выйти</a>
                </div>
            <? endif; ?>
        </div>
    </div>
</header>
<div class="content">
    <? $this->showContent(); ?>
</div>
</body>
</html>
