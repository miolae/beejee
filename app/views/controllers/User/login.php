<?php
/** @var \core\components\View $this */
/** @var bool $isGuest */
/** @var string $login */
/** @var string $error */

$this->setParent('main');
?>

<form action="/?c=user&a=login" method="post">
    <? if (!empty($error)): ?>
        <div class="row">
            <div class="col-xs-12 text-danger"><?= $error ?></div>
        </div>
    <? endif; ?>

    <div class="row">
        <div class="col-xs-12">
            <label>Логин:
                <input class="form-control" type="text" value="<?= htmlspecialchars($login); ?>" name="login" required>
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <label>Пароль:
                <input class="form-control" type="password" autocomplete="false" name="password" required>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <input type="submit" value="Войти" class="btn btn-success">
        </div>
    </div>
</form>
