<?php
/** @var bool $isGuest */
/** @var string $error */
/** @var \models\Task $task */
/** @var \core\components\View $this */

$this->setParent('main');

if (!empty($error)): ?>
    <div class="row">
        <div class="col-md-12 text-danger"><?= $error ?></div>
    </div>
<? endif;

if ( $isGuest && !empty($task->id) ):
    $this->subRender('', 404);
else:
    ?>

    <form action="/?c=task&a=edit&id=<?= htmlspecialchars($task->id) ?>" method="POST">
        <label>Имя пользователя
            <input type="text" name="author" value="<?= htmlspecialchars($task->author) ?>" required class="form-control">
        </label>
        <br>
        <label>E-mail
            <input type="email" name="email" value="<?= htmlspecialchars($task->email) ?>" required class="form-control">
        </label>
        <br>
        <label>Описание задачи
            <textarea name="message" cols="30" rows="10" required class="form-control"><?= htmlspecialchars($task->message) ?></textarea>
        </label>
        <br>
        <input type="submit" value="Сохранить" class="btn btn-success">
    </form>

<? endif; ?>
