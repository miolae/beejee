<?php
/** @var \models\Task[] $tasks */
/** @var bool $isGuest */

$this->setParent('main');
?>

<form action="/" method="get" class="sort-form">
    <div class="row">
        <div class="col-xs-12">Сортировать по:</div>
        <div class="col-xs-4">
            <select name="orderby[field]" class="form-cotrol">
                <option value="author">Пользователю</option>
                <option value="email">E-mail</option>
                <option value="ready">Статусу</option>
            </select>
        </div>
        <div class="col-xs-4">
            <select name="orderby[order]" class="form-cotrol">
                <option value="asc">По возрастанию</option>
                <option value="desc">По убыванию</option>
            </select>
        </div>
        <div class="col-xs-4">
            <input type="submit" value="Применить" class="btn btn-default">
        </div>
    </div>
</form>

<div class="row tasks-header">
    <div class="col-md-1">ID</div>
    <div class="col-md-4">Автор</div>
    <div class="col-md-4">E-mail</div>
    <div class="col-md-3">Статус</div>
    <?/* if ($isGuest): */?><!--
    <?/* else: */?>
        <div class="col-md-2">Статус</div>
        <div class="col-md-1"></div>
    --><?/* endif; */?>
</div>

<div class="row">
    <div class="col-md-12 task-list">
        <? foreach ($tasks as $task): ?>

            <div class="row task">
                <div class="col-md-1">
                    <a href="/?c=task&a=show&id=<?= $task->id ?>"><?= $task->id ?></a>
                </div>
                <div class="col-md-4"><?= htmlspecialchars($task->author) ?></div>
                <div class="col-md-4"><?= htmlspecialchars($task->email) ?></div>
                <div class="col-md-3"><?= $task->ready ? 'Готово' : 'В работе' ?></div>
                <?/* if ($isGuest): */?><!--
                <?/* else: */?>
                    <div class="col-md-2"><?/*= $task->ready ? 'Готово' : 'В работе' */?></div>
                    <div class="col-md-1">
                        <?/* if (!$task->ready): */?>
                            <a href="#" class="js-task-complete">Выполнить</a>
                        <?/* endif; */?>
                    </div>
                --><?/* endif; */?>
            </div>
        <? endforeach; ?>
    </div>
    <div class="col-md-12">
        <a href="?c=task&a=edit" class="btn btn-default btn-task-create">Создать задачу</a>
    </div>
</div>
