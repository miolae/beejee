<?php
/** @var bool $isGuest */
/** @var \models\Task $task */
/** @var \core\components\View $this */

$this->setParent('main');
?>

<div class="row">
    <div class="col-md-12">
        <div class="row tasks-header">
            <div class="col-md-4">Пользователь</div>
            <div class="col-md-4">E-mail</div>
            <div class="col-md-4">Статус</div>
        </div>
        <div class="row">
            <div class="col-md-4"><?= htmlspecialchars($task->author) ?></div>
            <div class="col-md-4"><?= htmlspecialchars($task->email) ?></div>
            <div class="col-md-4"><?= $task->ready ? 'Завершено' : 'В работе' ?></div>
        </div>
        <hr>
        <div class="row tasks-header">
            <div class="col-md-12">Описание задачи</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--<img src="<? /*= $task->image */ ?>" alt="" class="pull-left">-->
                <?= htmlspecialchars($task->message) ?>
            </div>
        </div>

        <? if ($task->id && !$isGuest): ?>
            <div class="row">
                <div class="col-xs-12">
                    <a href="/?c=task&a=edit&id=<?= $task->id ?>" class="btn btn-default pull-right">Редактировать</a>
                </div>
            </div>
        <? endif; ?>
    </div>
</div>
