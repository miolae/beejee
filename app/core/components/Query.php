<?php

namespace core\components;

use core\Application;
use core\interfaces\QueryInterface;

class Query implements \core\interfaces\QueryInterface
{
    protected $select;
    protected $table;
    protected $where;
    protected $order;
    protected $limit;
    protected $offset;
    protected $sql;
    protected $params = [];
    /** @var Model $model */
    protected $model;

    public function __construct($class = false)
    {
        if ($class) {
            $this->model = $class;
            $this->from($this->model::tableName());
        }
    }

    public function one()
    {
        $this->buildQuery();
        $result = $this->execute()->fetch(\PDO::FETCH_ASSOC);
        if ($result && $this->model) {
            /** @var Model $model */
            $model = new $this->model;
            $result = $model->load($result, true);
        }

        return $result;
    }

    public function all()
    {
        $this->buildQuery();
        $result = $this->execute()->fetchAll(\PDO::FETCH_ASSOC);

        if ($this->model) {
            $resultNew = [];
            foreach ($result as $item) {
                /** @var Model $model */
                $model = new $this->model;
                $resultNew[] = $model->load($item, true);
            }
            $result = $resultNew;
        }

        return $result;
    }

    public function execute(): \PDOStatement
    {

        $pdo = Application::$instance->db->connection;
        $q = $pdo->prepare($this->sql);
        $q->execute($this->params);

        return $q;
    }

    /**
     * @param string|array $select
     * @return QueryInterface
     */
    public function select($select): Query
    {
        $this->sql = false;

        if (is_array($select)) {
            $select = implode(', ', $select);
        }
        $this->select = $select;


        return $this;
    }

    public function from(string $table): Query
    {
        $this->sql = false;

        $this->table = $table;

        return $this;
    }

    public function where(string $where): Query
    {
        $this->sql = false;

        $this->where = "WHERE $where";

        return $this;
    }

    public function limit(int $count): Query
    {
        $this->sql = false;

        if ($count > 0) {
            $this->limit = 'LIMIT ' . $count;
        } else {
            $this->limit = false;
        }

        return $this;
    }

    public function offset(int $count): Query
    {
        $this->sql = false;

        if ($count > 0) {
            $this->offset = 'OFFSET ' . $count;
        } else {
            $this->offset = false;
        }

        return $this;
    }

    public function order(?string $field, ?string $order): Query
    {
        $this->sql = false;

        if ($field) {
            $this->order = "ORDER BY $field";

            if ($order) {
                $this->order .= ' ' . $order;
            }
        } else {
            $this->order = false;
        }

        return $this;
    }

    protected function buildQuery(): string
    {
        if (empty($this->table)) {
            throw new \Exception('Trying to build a query with empty table');
        }

        if (empty($this->select)) {
            $this->select = '*';
        }

        $this->sql = "SELECT $this->select FROM $this->table $this->where $this->order $this->limit $this->offset;";

        return $this->sql;
    }

    public function getSql(): string
    {
        return $this->sql;
    }

    public function setSql(string $sql): Query
    {
        $this->sql = $sql;

        return $this;
    }

    public function addParameters(array $params): Query
    {
        $this->params = array_merge($this->params, $params);

        return $this;
    }

    public function removeParameters(array $params): Query
    {
        foreach ($params as $param) {
            if (array_key_exists($param, $this->params)) {
                unset($this->params[ $param ]);
            }
        }

        return $this;
    }

    public function clearParameters(): Query
    {
        $this->params = [];

        return $this;
    }
}
