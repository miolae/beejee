<?php

namespace core\components;

use core\Component;
use core\exceptions\UnknownFieldException;

abstract class Model extends Component
{
    protected $properties = [];
    protected $propsOriginal = [];

    abstract public static function tableName(): string;

    public static function find(): Query
    {
        return new Query(static::className());
    }

    public function load(array $properties, $internal = false)
    {
        $properties['id'] = (int)$properties['id'];

        $this->properties = $properties;
        if ($internal) {
            $this->propsOriginal = $properties;
        }

        return $this;
    }

    public function save()
    {
        if (!empty(array_diff($this->properties, $this->propsOriginal))) {
            if (empty($this->id)) {
                return $this->update();
            } else {
                return $this->insert();
            }
        }

        return true;
    }

    protected function insert()
    {
        $table = $this->tableName();
        $q = new Query();
        $columns = implode(', ', array_keys($this->properties));
        $props = [];
        foreach ($this->properties as $key => $value) {
            $props[':' . $key] = $value;
        }
        $binds = implode(', ', array_keys($props));

        /** @var \PDOStatement $result */
        $result = $q
            ->setSql("INSERT INTO $table ($columns) VALUES ($binds);")
            ->addParameters($props)
            ->execute();

        return $result->rowCount();
    }

    protected function update()
    {
        $table = $this->tableName();
        $q = new Query();
        $columns = array_diff($this->propsOriginal, $this->properties);
        $props = [];
        $update = '';
        foreach ($columns as $key => $value) {
            if (strlen($update)) {
                $update .= ', ';
            }
            $update .= "$key = :$key";
            $props[':' . $key] = $value;
        }
        $props[':id'] = $this->id;

        /** @var \PDOStatement $result */
        $result = $q->setSql("UPDATE $table SET $update WHERE id = :id;")->addParameters($props)->execute();

        return $result->rowCount();
    }

    public function __get($name)
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name];
        }

        if (method_exists($this, "get$name")) {
            return call_user_func([$this, "get$name"]);
        }

        throw new UnknownFieldException($name);
    }

    public function __set($name, $value)
    {
        if ($name === 'id') {
            throw new \Exception('Can\'t set primary key');
        }

        $this->properties[$name] = $value;
    }
}
