<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 19:09
 */

namespace core\components;


use core\Application;
use core\exceptions\ViewNotFoundException;

class View
{
    protected $parent;
    protected $content = '';
    protected $assets = [
        'css' => [],
        'js'  => [],
    ];

    protected function showAssets()
    {
        foreach ($this->assets['css'] as $style) {
            $file = self::getAssetPublicPath($style);
            echo "<link href='$file' rel='stylesheet' type='text/css'>";
        }

        foreach ($this->assets['js'] as $script) {
            $file = self::getAssetPublicPath($script);
            echo "<script href='$file' type='text/javascript'>";
        }
    }

    public function addCss($filename)
    {
        if ($this->needAsset($filename, 'css')) {
            $this->assets['css'][] = $filename;
        }
    }

    protected function needAsset($filename, $type)
    {
        $file = self::getAssetPath($filename);

        return file_exists($file) && !in_array($file, $this->assets[ $type ]);
    }

    protected static function getAssetPath($filename)
    {
        $sep = DIRECTORY_SEPARATOR;
        return Application::$instance->getBasePath() . "{$sep}assets{$sep}$filename";
    }

    public function addJs($filename)
    {
        if ($this->needAsset($filename, 'js')) {
            $this->assets['js'][] = $filename;
        }
    }

    public function getAssets()
    {
        return $this->assets;
    }

    protected function setParent(string $parent): void
    {
        $this->parent = $parent;
    }

    protected function showContent(): void
    {
        echo $this->content;
    }

    protected function subRender(string $path, string $view, array $parameters = []): void
    {
        $obj = new static;
        $obj->render($path, $view, $parameters);

        foreach ($obj->assets['css'] as $style) {
            $this->addCss($style);
        }

        foreach ($obj->assets['js'] as $script) {
            $this->addJs($script);
        }
    }

    public function render(string $path, string $view, array $parameters = []): void
    {
        $sep = DIRECTORY_SEPARATOR;
        if (strpos($path, '\\') === 0) {
            $path = substr($path, 1);
        }
        $path = array_filter(explode('\\', $path));
        $path = 'views' . $sep . implode($sep, $path);

        if (strpos($view, 'action') === 0) {
            $view = substr($view, 6);
        }

        $view = preg_replace("/([A-Z])/", '-$1', $view);
        $view = strtolower(trim($view, '-'));

        $filename = Application::$instance->getBasePath() . $sep . $path . $sep . $view . '.php';

        if (file_exists($filename)) {
            extract($parameters, EXTR_OVERWRITE);

            ob_start();
            require $filename;
            $this->content = ob_get_clean();

            if (!empty($this->parent)) {
                $parent = $this->parent;
                unset($this->parent);
                $this->render('layouts', $parent);
            }

            echo $this->content;
            $this->content = '';

        } else {
            throw new ViewNotFoundException($filename);
        }
    }

    protected static function getAssetPublicPath($filename)
    {
        $sep = DIRECTORY_SEPARATOR;
        $basePath = Application::$instance->getBasePath();
        $publicDirectory = "upload{$sep}assets";
        $publicPath = $publicDirectory . $sep . $filename;
        $publicPathFull = "$basePath$sep..{$sep}$publicPath";
        $actual = self::getAssetPath($filename);

        if (!is_dir($publicDirectory)) {
            mkdir($publicDirectory, 0755, true);
        }

        if (!file_exists($publicPathFull) || (md5(file_get_contents($publicPathFull)) !== md5(file_get_contents($actual)))) {
            copy($actual, $publicPathFull);
        }

        return $publicPath . '?' . filemtime($publicPathFull);
    }
}
