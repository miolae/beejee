<?php

namespace core\components;

use core\Component;
use core\exceptions\RouteNotFoundException;
use core\interfaces\RouterInterface;

class Router extends Component implements RouterInterface
{
    protected $config = [
        'controllerVariable' => 'c',
        'actionVariable' => 'a',
    ];
    protected $controllerVariable;
    protected $actionVariable;

    public function init()
    {
        $this->controllerVariable = $this->config['controllerVariable'];
        $this->actionVariable = $this->config['actionVariable'];
    }

    public function routeExecute()
    {
        $action = $this->getAction();
        $controller = $this->getController();


        if ($controller && class_exists($controller) && $action) {
            $controller = new $controller;

            $isController = $controller instanceof Controller;
            $methodExists = method_exists($controller, $action);

            if ( $isController && $methodExists ) {
                return call_user_func([$controller, $action]);
            }
        }

        throw new RouteNotFoundException($controller, $action);
    }

    public function getController(): string
    {
        $controller = $_GET[ $this->controllerVariable ];
        if (empty($controller) && !empty($this->config['controllerDefault'])) {
            $controller = $this->config['controllerDefault'];
        }

        if ($controller) {
            $controller = self::nameNormalize($controller);

            return "controllers\\$controller";
        }

        return $controller;
    }

    public function getAction(): string
    {
        $action = $_GET[ $this->actionVariable ];
        if (empty($action) && !empty($this->config['actionDefault'])) {
            $action = $this->config['actionDefault'];
        }

        if ($action) {
            $action = self::nameNormalize($action);

            return "action$action";
        }

        return $action;
    }

    protected static function nameNormalize($name) {
        $name = explode('-', $name);
        $name = array_map('ucfirst', $name);
        $name = implode('', $name);

        return $name;
    }
}
