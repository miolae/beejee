<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 20:31
 */

namespace core\components;

use core\Component;

/**
 * @property \PDO $connection
 */
class DB extends Component
{
    protected $connection;

    public function getConnection()
    {
        if (!$this->connection) {
            $cnf = $this->config;
            $dsn = "mysql:host={$cnf['host']};dbname={$cnf['db']}";
            $this->connection = new \PDO($dsn, $cnf['user'], $cnf['password']);
        }

        return $this->connection;
    }
}
