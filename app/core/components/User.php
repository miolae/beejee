<?php
/**
 * Created by PhpStorm.
 * User: miolae
 * Date: 17.07.17
 * Time: 11:51
 */

namespace core\components;


use core\Component;
use core\interfaces\UserInterface;
use models\User as UserModel;

/**
 * @property Model $model
 */
class User extends Component implements UserInterface
{
    protected const SESSION_UID = 'uid';
    protected const SESSION_EXPIRE = 'uid_expire';
    /** @var UserModel */
    protected $data;

    public function isGuest()
    {

        $expire = (int)$_COOKIE[ self::SESSION_EXPIRE ];
        if ($expire <= 0) {
            $expire = (int)$_SESSION[ self::SESSION_EXPIRE ];
        }
        if ($expire > 0 && time() > $expire) {
            $this->logout();

            return true;
        }

        $id = (int)$_COOKIE[ self::SESSION_UID ];
        if ($id <= 0 && $_SESSION[ self::SESSION_UID ] > 0) {
            $id = (int)$_SESSION[ self::SESSION_UID ];
        }

        $model = UserModel::find()->where("id = $id")->one();
        if ($model) {
            $this->data = $model;
            $this->login();

            return false;
        } else {
            $this->logout();

            return true;
        }
    }

    public function getModel()
    {
        return $this->data;
    }

    public function authorize($login, $password)
    {
        $model = UserModel::find()->where('login = :login and passwd = :pass')->addParameters([
                ':login' => $login,
                ':pass'  => $password,
            ])->one();

        if ($model) {
            $this->data = $model;
            $this->login();

            return true;
        } else {
            $this->logout();

            return false;
        }
    }

    protected function login()
    {
        $_SESSION[ self::SESSION_UID ] = $this->data->id;
        setcookie(self::SESSION_UID, $this->data->id, self::getExpires());
    }

    protected static function getExpires()
    {
        return time() + 60 * 30;
    }

    public function logout()
    {
        unset($_SESSION[ self::SESSION_UID ]);
        setcookie(self::SESSION_UID, false, -1);
    }
}
