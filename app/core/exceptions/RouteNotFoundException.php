<?php

namespace core\exceptions;

class RouteNotFoundException extends \Exception
{
    public function __construct(?string $controller, ?string $action)
    {
        $message = "Route not found (controller `$controller` and action `$action` are expected)";
        parent::__construct($message);
    }
}
