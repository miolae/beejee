<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 15:51
 */

namespace core\exceptions;


class UnknownFieldException extends \Exception
{
    public function __construct($field)
    {
        $message = "Getting unknown field `$field`";
        parent::__construct($message);
    }
}
