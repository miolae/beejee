<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 15:45
 */

namespace core\exceptions;


class ClassNotFoundException extends \Exception
{
    public function __construct($className)
    {
        $message = "Class `$className` not found";
        parent::__construct($message);
    }
}
