<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 15:45
 */

namespace core\exceptions;


class ViewNotFoundException extends \Exception
{
    public function __construct(?string $file)
    {
        $message = "View `$file` not found";
        parent::__construct($message);
    }
}
