<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 15:34
 */

namespace core;


class Component
{
    protected $config;

    public function __construct(Array $config = [])
    {
        $this->config = \helpers\ArrayHelper::merge($this->config, $config);
        $this->init();
    }

    public function init(){}

    public static function className()
    {
        return get_called_class();
    }

    public function __get($name)
    {
        if (method_exists($this, "get$name")) {
            return call_user_func([$this, "get$name"]);
        }
    }
}
