<?php
/**
 * Created by PhpStorm.
 * User: miolae
 * Date: 17.07.17
 * Time: 11:52
 */

namespace core\interfaces;


interface UserInterface
{
    public function isGuest();
    public function getModel();
}
