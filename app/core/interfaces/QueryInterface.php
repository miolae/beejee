<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 22:24
 */

namespace core\interfaces;


interface QueryInterface
{
    /** @return array|false */
    public function one();
    /** @return array|false */
    public function all();
    public function execute();
    /** @param string|array $select */
    public function select($select);
    public function from(string $table);
    public function where(string $condition);
    public function limit(int $count);
    public function offset(int $count);
    public function order(?string $field, ?string $order);
    public function getSql():string;
    public function setSql(string $sql);
}
