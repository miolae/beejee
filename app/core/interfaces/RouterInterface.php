<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 16.07.2017
 * Time: 16:44
 */

namespace core\interfaces;


interface RouterInterface
{
    public function routeExecute();
    public function getController():string;
    public function getAction():string;
}
