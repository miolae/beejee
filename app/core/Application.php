<?php

namespace core;

use core\components\User;
use core\components\View;
use core\exceptions\RouteNotFoundException;
use core\exceptions\UnknownFieldException;
use core\exceptions\ViewNotFoundException;
use core\interfaces\RouterInterface;

/**
 * @property \core\components\DB $db
 * @property RouterInterface     $router
 * @property View                $view
 * @property User                $user
 */
class Application extends Component {
    /** @var Application $instance */
    public static $instance;

    protected $config = [
        'components' => [
            'db'     => [
                'class' => 'core\\components\\DB',
                'host'  => 'localhost',
            ],
            'router' => [
                'class' => 'core\\components\\Router',
            ],
            'view'   => 'core\\components\\View',
            'user'   => 'core\\components\\User',
        ],
    ];

    /** @var Component[] $components */
    private $components = [];
    private $basePath;

    public function init() {
        self::$instance = $this;

        $basePath = explode(DIRECTORY_SEPARATOR, __DIR__);
        array_pop($basePath);
        $this->basePath = implode(DIRECTORY_SEPARATOR, $basePath);
    }

    public function run() {
        session_start();
        $this->handleRequest();
    }

    public function __get($name) {

        if ( isset($this->components[ $name ]) ) {
            return $this->components[ $name ];
        } elseif ( isset($this->config['components'][ $name ]) ) {
            $config = $this->config['components'][ $name ];
            $class  = '';

            if ( is_string($config) ) {
                $class  = $config;
                $config = [];
            } elseif ( isset($config['class']) ) {
                $class = $config['class'];
                unset($config['class']);
            }
            if ( ! empty($class) && class_exists($class) ) {
                /** @var Component $component */
                $component                 = new $class($config);
                $this->components[ $name ] = $component;

                return $this->components[ $name ];
            }

        }

        throw new UnknownFieldException($name);
    }

    public static function redirect($url) {
        header("Location: $url");
        die;
    }

    public function getBasePath() {
        return $this->basePath;
    }

    private function handleRequest() {

        try {
            $result = $this->router->routeExecute();

            if ( is_array($result) ) {
                $this->view->render($this->router->getController(), $this->router->getAction(), $result);
            }

        } catch (RouteNotFoundException|ViewNotFoundException $e) {
            $this->view->render('', 404);
        }
    }

}
