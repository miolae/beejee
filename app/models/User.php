<?php

namespace models;


use core\components\Model;

/**
 * @property integer $id
 * @property string  $login
 * @property string  $passwd
 */
class User extends Model
{
    public static function tableName(): string
    {
        return 'users';
    }
}
