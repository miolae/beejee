<?php
/**
 * Created by PhpStorm.
 * User: miola
 * Date: 17.07.2017
 * Time: 2:45
 */

namespace models;

use core\components\Model;

/**
 * @property int id
 * @property bool ready
 * @property string email
 * @property string author
 * @property string message
 * @property string image
 */
class Task extends Model
{

    public function init()
    {
        $this->load([
            'id' => '',
            'author' => '',
            'email' => '',
            'message' => '',
            'ready' => '',
        ], true);
    }

    public static function tableName(): string
    {
        return 'tasks';
    }

    public function load(array $properties, $internal = false)
    {
        $properties['ready'] = (bool)$properties['ready'];

        return parent::load($properties, $internal);
    }
}
