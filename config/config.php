<?php

return [
    'components' => [
        'db' => require __DIR__ . '/db.php',
        'router' => [
            'controllerDefault' => 'task',
            'actionDefault' => 'index',
        ],
    ],
];
