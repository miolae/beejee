<?php

spl_autoload_register(function ($class) {
    $sep = DIRECTORY_SEPARATOR;
    $base_dir = __DIR__ . $sep . '..' . $sep . 'app';
    $file = $base_dir . $sep . str_replace('\\', $sep, $class) . '.php';

    if (file_exists($file)) {
        require_once $file;
    }
});
