<?php

use core\Application;

require_once __DIR__ . '/config/autoload.php';
$config = require_once __DIR__ . '/config/config.php';

(new Application($config))->run();
